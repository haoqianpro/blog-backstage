import { connection } from '../mysql.js'

class Article {
  static limit(offset,length,cb) {
    connection.query('SELECT * FROM `articles` LIMIT '+offset+', '+length, cb)
  }
  static all(cb) {
    connection.query('SELECT * FROM `articles`', cb)
  }
  static allByDate(cb) {
    connection.query('SELECT * FROM `articles` ORDER BY `createTime` DESC ', cb)
  }
  static find(id, cb) {
    connection.query('SELECT * FROM `articles` WHERE `id` = ?', id, cb)
  }

  static create(data, cb) {
    const sql = 'INSERT INTO `articles`(`title`,`content`,`category`,`thumbnail`) VALUES(?,?,?,?)'
    connection.execute(sql,[data.title,data.content,data.category,data.thumbnail], cb)
  }

  static update(data, cb) {
    const sql = 'UPDATE `articles` SET `title`=?, `content`=?, `category`=? WHERE `id`=?'
    connection.execute(sql,[data.title,data.content,data.category,data.id],cb)
  }

  static remove(id, cb) {
    connection.execute('DELETE FROM `articles` WHERE `id` =?',[id],cb)
  }
}

export default Article