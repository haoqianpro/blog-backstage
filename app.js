import express from 'express'
import cors from 'cors'
import morgan from 'morgan'
import fs from 'fs'
import { APP_PORT } from './app.config.js'
import articleRoute from './routers/articles.js'

const app = express()
app.use(cors())
app.use(morgan('common'))
app.use('/upload', express.static('./upload'))

app.use('/articles', articleRoute)

app.use((err,req,res) => {
  if (err) {
    fs.appendFile('./log.txt', '\n'+err, err => {
      if (err) console.log(err)
    })
    res.send({status: 'error'})
  } else res.send({status: 'error'})
})

app.listen(APP_PORT, () => console.log(`server running at http://localhost:${APP_PORT}`))